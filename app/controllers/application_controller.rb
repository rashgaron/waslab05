class ApplicationController < ActionController::Base
  add_flash_types :success, :error

rescue_from ActionController::InvalidAuthenticityToken, with: :redirect_to_referer_or_path

  def redirect_to_referer_or_path
    flash[:error] = "La cache se ha borrado junto al AuthenticityToken, refresca la pagina"
    redirect_to tweets_url
  end
  
end
