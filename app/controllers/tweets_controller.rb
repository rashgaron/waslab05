class TweetsController < ApplicationController
  before_action :set_tweet, only: [:show, :edit, :update, :destroy, :like]
  add_flash_types :success, :error
  # GET /tweets
  # GET /tweets.json
  def index
    @tweets = Tweet.all.order(created_at: :desc)
    @newTweet= Tweet.new
  end

  # GET /tweets/1
  # GET /tweets/1.json
  def show
  end

  # GET /tweets/new
  def new
    @tweet = Tweet.new
  end

  # GET /tweets/1/edit
  def edit
  end

  # POST /tweets
  # POST /tweets.json
  def create
    @tweet = Tweet.new(tweet_params)
    @tweet.likes = 0
    respond_to do |format|
      if @tweet.save
        unless session[:created_ids].nil?
          session[:created_ids] = session[:created_ids] << @tweet.id
        else
          session[:created_ids] = [@tweet.id]
        end

        format.html { redirect_to tweets_url, success: 'Tweet was successfully created.' }
        format.json { render :show, status: :created, location: @tweet }
      else
        errors = []
        @tweet.errors.each do |error|
          errors << error.full_message 
        end
        format.html { redirect_to tweets_url, error: errors }
        format.json { render json: @tweet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tweets/1
  # PATCH/PUT /tweets/1.json
  def update
    respond_to do |format|
      if @tweet.update(tweet_params)
        format.html { redirect_to @tweet, success: 'Tweet was successfully updated.' }
        format.json { render :show, status: :ok, location: @tweet }
      else
        format.html { render :edit }
        format.json { render json: @tweet.errors, status: :unprocessable_entity }
      end
    end
  end

  def like
    @tweet.likes = @tweet.likes + 1
    @tweet.save

    respond_to do |format|
      format.html { redirect_to tweets_url, success: 'Like <3 !' } 
      format.json { head :ok } 
    end
  end

  # DELETE /tweets/1
  # DELETE /tweets/1.json
  def destroy

    respond_to do |format|
      if session[:created_ids].nil? || session[:created_ids].exclude?(@tweet.id)
          format.html { redirect_to tweets_url, error: 'You are not allowed to delete this tweet.' }
          format.json { head :forbidden }   
      else 
        @tweet.destroy
        session[:created_ids].delete(@tweet.id)
        format.html { redirect_to tweets_url, success: 'Tweet was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tweet
      @tweet = Tweet.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def tweet_params
      params.require(:tweet).permit(:author, :content)
    end
end
