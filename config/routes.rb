Rails.application.routes.draw do
  resources :tweets
  put '/tweets/:id/like', to: 'tweets#like', as: 'like_tweet'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'tweets#index'

end
